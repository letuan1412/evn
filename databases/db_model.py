import os
import sys
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, String, Unicode, Integer, DateTime, BigInteger, Date, DECIMAL, VARCHAR, INT, TEXT, NVARCHAR, BOOLEAN
from sqlalchemy.dialects.mysql import DOUBLE, DATETIME, TINYINT, INTEGER, FLOAT

Base = declarative_base()
ACCESS_FILE_PATH = os.path.dirname(os.path.abspath(__file__)) + '/access.txt'


class MapId(Base):
    __tablename__ = 'MapId'
    Id = Column(INT, primary_key=True, autoincrement=True)
    RequestId = Column(VARCHAR(100), nullable=False)
    IssueId = Column(VARCHAR(100), nullable=False)
    DOfficeId = Column(VARCHAR(100), nullable=False)

    def __init__(self, RequestId, TicketId, DOfficeId):
        self.RequestId = RequestId
        self.TicketId = TicketId
        self.DOfficeId = DOfficeId


def read_file_config():
    result = {}
    f = open(ACCESS_FILE_PATH, 'r')
    for line in f:
        temp = line.split(':')
        result[temp[0].strip()] = temp[1].strip()
    f.close()
    return result

CONFIG_DIC = read_file_config()


def get_engine():
    user_name = CONFIG_DIC['user']
    password = CONFIG_DIC['password']
    host = CONFIG_DIC['host']
    db_name = CONFIG_DIC['database']
    port = CONFIG_DIC['port']
    mysql_engine_str = 'mssql+pyodbc://%s:%s@%s:%s/%s?driver=ODBC+Driver+17+for+SQL+Server' % (user_name, password, host, port, db_name)
    engine = create_engine(mysql_engine_str, pool_recycle=3600 * 7)
    return engine

def create_database():
    engine = get_engine()
    Base.metadata.create_all(engine)

# if __name__ == '__main__':
#     create_database()
