from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import desc, or_, asc, extract

from db_model import get_engine, MapId

from db_log import error

Base = declarative_base()
ENGINE = get_engine()
Base.metadata.bind = ENGINE
DBSession = sessionmaker(bind=ENGINE)

# INSERT METHOD ================

def insert_object(object):
    session = DBSession()
    try:
        session.add(object)
        session.commit()
    except Exception as e:
        error('error at inserting object')
        error('error info: %s' % str(e))
        error('========================***========================')
        session.rollback()
    finally:
        session.close()

def insert_list_object(list_object):
    session = DBSession()
    try:
        session.bulk_save_objects(list_object)
        session.commit()
    except Exception as e:
        error('error at inserting %d objects' % len(list_object))
        error('error info: %s' % str(e))
        error('========================***========================')
        session.rollback()
    finally:
        session.close()

# GET METHOD =====================

def get_map_by_request(id):
    session = DBSession()
    try:
        query = session.query(MapId).filter_by(RequestId=id)
        records = query.all()
    except Exception as e:
        error('error info: %s' % str(e))
        error('error at get_map_by_request')
        error('========================***========================')
        records = []
    finally:
        session.close()
    return records


# UPDATE METHOD =====================



