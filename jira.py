import requests
from requests.auth import HTTPBasicAuth
import sys
import json
import os
import datetime

BASER_DIR = os.path.abspath(os.path.dirname(__file__))
sys.path.append(BASER_DIR)
DATABASE_DIR = os.path.join(BASER_DIR, "databases")
sys.path.append(DATABASE_DIR)

from databases.db_model import MapId
from databases.db_session import insert_object

url = "http://jira.evn.com.vn/rest/api/2/issue"

# auth = HTTPBasicAuth("SDP_Jira", "U0RQX0ppcmE6SEdmQCMxMjQ=")

headers = {
  'Authorization': 'Basic U0RQX0ppcmE6SEdmQCMxMjQ=',
  'Content-Type': 'application/json',
                  'User-Agent': 'PostmanRuntime/7.28.4',
}


class Jira(object):
    def __init__(self, data):
        self.input_data = data
        print(type(data))
        # self.data = self.request2Issues(self.input_data)

    # def createTicket(self):
    #     print(self.data)
    #     url = HOST + "/api/v1/tickets"
    #     access_token = self.get_access_token()
    #     print("====ok2======= " + str(access_token))
    #     headers = {
    #         "Content-Type": "application/json",
    #         "orgId": ORG_ID,
    #         "Authorization": "Zoho-oauthtoken " + access_token
    #     }
    #     try:
    #         response = requests.post(url, data=json.dumps(self.data), headers=headers)
    #         if response.status_code == 200:
    #             response_json = json.loads(response.text)
    #             request_id = self.data.get("cf").get("cf_itsm_id")
    #             ticket_id = response_json.get("id")
    #             map = MapId(request_id, ticket_id, None)
    #             insert_object(map)
    #             return ticket_id
    #         elif response.status_code == 422:
    #             result = self.refresh_token()
    #             if result is not None:
    #                 access_token = self.get_access_token()
    #                 headers = {
    #                     "Content-Type": "application/json",
    #                     "orgId": ORG_ID,
    #                     "Authorization": "Zoho-oauthtoken " + access_token
    #                 }
    #                 response = requests.post(url, data=self.data, headers=headers)
    #                 if response.status_code == 200:
    #                     response_json = json.loads(response.text)
    #                     request_id = self.data.get("cf").get("cf_itsm_id")
    #                     ticket_id = response_json.get("id")
    #                     map = MapId(request_id, ticket_id, None)
    #                     insert_object(map)
    #                     return ticket_id
    #     except Exception as e:
    #         print(e)
    #     return None
    #
    # def updateTicket(self):
    #     request_id = self.data.get("cf").get("cf_itsm_id")
    #     records = get_map_by_request(str(request_id))
    #     if len(records) == 0:
    #         return None
    #     print("request_id")
    #     print(request_id)
    #     ticket_id = records[0].TicketId
    #     print(ticket_id)
    #     url = HOST + "/api/v1/tickets/" + ticket_id
    #     access_token = self.get_access_token()
    #     headers = {
    #         "Content-Type": "application/json",
    #         "orgId": ORG_ID,
    #         "Authorization": "Zoho-oauthtoken " + access_token
    #     }
    #     try:
    #         response = requests.patch(url, data=json.dumps(self.data), headers=headers)
    #         if response.status_code == 200:
    #             return response.text
    #         elif response.status_code == 422:
    #             result = self.refresh_token()
    #             if result is not None:
    #                 access_token = self.get_access_token()
    #                 headers = {
    #                     "Content-Type": "application/json",
    #                     "orgId": ORG_ID,
    #                     "Authorization": "Zoho-oauthtoken " + access_token
    #                 }
    #                 response = requests.post(url, data=self.data, headers=headers)
    #                 if response.status_code == 200:
    #                     return response.text
    #     except Exception as e:
    #         print(e)
    #     return None

    def createIssue(self):
        url_create = "http://jira.evn.com.vn/rest/api/2/issue"
        payload = self.request2Issues(self.input_data)
        response = requests.request(
            "POST",
            url_create,
            data=payload,
            headers=headers
        )
        if response.status_code == 200:
            print(response.text)
            response_json = json.loads(response.text)
            requestId = ""
            issuesId = ""
            dOfficeId = ""
            map = MapId(requestId, issuesId, dOfficeId)
            insert_object(map)

    def updateIssue(self):
        url_create = "http://jira.evn.com.vn/rest/api/2/issue"
        payload = self.request2Issues(self.input_data)
        response = requests.request(
            "POST",
            url_create,
            data=payload,
            headers=headers
        )
        if response.status_code == 200:
            print(response.text)
            response_json = json.loads(response.text)
            requestId = ""
            issuesId = ""
            dOfficeId = ""
            map = MapId(requestId, issuesId, dOfficeId)
            insert_object(map)

    def deleteIssue(self):
        url_create = "http://jira.evn.com.vn/rest/api/2/issue"
        payload = self.request2Issues(self.input_data)
        response = requests.request(
            "POST",
            url_create,
            data=payload,
            headers=headers
        )
        if response.status_code == 200:
            print(response.text)
            response_json = json.loads(response.text)
            requestId = ""
            issuesId = ""
            dOfficeId = ""
            map = MapId(requestId, issuesId, dOfficeId)
            insert_object(map)

    def request2Issues(self, input_data):
        subject = input_data.get("subject")
        description = input_data.get("description")
        short_description = input_data.get("short_description")
        id = input_data.get("id")
        udf_fields = input_data.get("udf_fields")
        # requester = input_data.get("requester")  # => name, email_id, mobile
        status = input_data.get("status")  # => name


        udf_fields_json = udf_fields
        project = None
        issue_type = None
        phan_loai_chi_tiet_yc = None
        cmis_30 = None
        sub_phan_he = None
        summary = None
        don_vi_kh = None
        sub_don_vi_kh = None
        thong_tin_jira = None
        sdt = None
        nguon_yc = None
        nguoi_tiep_nhan = None
        components = None
        giai_doan = None
        if udf_fields_json is not None:
            project = udf_fields_json.get("udf_sline_623") # project
            issue_type = udf_fields_json.get("udf_sline_624") # issues type
            phan_loai_chi_tiet_yc = udf_fields_json.get("udf_sline_625") # phan loai chi tiet yc
            cmis_30 = udf_fields_json.get("udf_sline_626") # cmis 3.0
            sub_phan_he = udf_fields_json.get("udf_sline_627") # sub phan he
            summary = udf_fields_json.get("udf_sline_628") # summary
            don_vi_kh = udf_fields_json.get("udf_sline_629") # don vi KH
            sub_don_vi_kh = udf_fields_json.get("udf_sline_630") # sub don vi KH
            thong_tin_jira = udf_fields_json.get("udf_sline_631") # thong tin lien he jira
            sdt = udf_fields_json.get("udf_sline_632") # sdt
            nguon_yc = udf_fields_json.get("udf_sline_633") # nguon yc
            nguoi_tiep_nhan = udf_fields_json.get("udf_date_634") # nguoi tiep nhan
            components = udf_fields_json.get("udf_sline_635") # components
            giai_doan = udf_fields_json.get("udf_sline_636") # giai doan phat sinh issues

        status_name = None
        if status is not None:
            status_name = status.get("name")
        payload = json.dumps({
            "update": {},
            "fields": {
                "summary": summary,
                "issuetype": {
                    "id": "13620"
                },
                "components": [
                ],
                "customfield_11761": {
                    "id": "17713",
                    "value": "-1"
                },
                "customfield_13260":  {"value" : "CM - Component dùng chung", "child": {"value":"CM.01 - TimYeuCau"}},
                "project": {
                    "id": "17831"
                },
                "description": short_description,
                "customfield_10012": {"value": "Cơ quan Tập đoàn", "child": {"value":"HĐTV"}},
                "customfield_10014": thong_tin_jira,
                "customfield_16960": int(sdt),
                "customfield_11060": {"id": "14513","value":"1"},
                # "customfield_10015": {"id": "14513","value":"1"},
            }
        })

        return payload
