import sys
import os
BASER_DIR = os.path.abspath(os.path.dirname(__file__))
sys.path.append(BASER_DIR)
DATABASE_DIR = os.path.join(BASER_DIR, "databases")
sys.path.append(DATABASE_DIR)

from flask import Flask, request, jsonify
import json
import requests
from datetime import datetime
from jira import Jira

app = Flask(__name__)

@app.route("/jira/update", methods = ['POST'])
def update_jira1():
    update_data = request.json
    print("jira updated")
    print(update_data)
    return ""

@app.route("/jira/updated", methods = ['POST'])
def update_jira():
    update_data = request.json
    print("jira updated")
    print(update_data)
    return ""


@app.route("/sdp/created", methods = ['POST'])
def create_request():
    print("sdp created")
    input_data = request.form
    data = input_data.get("input_data")
    print(type(data))
    obj = Jira(json.loads(data))
    obj.createIssue()
    return ""


def get_product(product_name):
    pass

if __name__ == "__main__":
    app.debug = True
    app.run(host='0.0.0.0', port=8080)