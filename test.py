import requests
from requests.auth import HTTPBasicAuth
import json

url = "http://jira.evn.com.vn/rest/api/3/field/customfield_11761/contexts"

auth = HTTPBasicAuth("SDP_Jira", "cfad0e4c41db1d94b8e4e01e3ea84a622500cd37")

headers = {
   "Accept": "application/json"
}

response = requests.request(
   "POST",
   url,
   headers=headers,
   auth=auth
)
print(response.text)

print(json.dumps(json.loads(response.text), sort_keys=True, indent=4, separators=(",", ": ")))